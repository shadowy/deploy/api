# CHANGELOG

<!--- next entry here -->

## 0.1.4
2021-03-11

### Fixes

- fix ci/cd (35c5011ee790ae9daf2773d550d9ee6fa203c47a)

## 0.1.3
2021-03-11

### Fixes

- fix Dockerfile (77fcd4f2d3ff4f5a5d6b13d474fdee3fa637e12a)

## 0.1.2
2021-03-11

### Fixes

- ci/cd (30b1c52d90840a66a38d9cc1463083692f6bf8c8)
- ci/cd (dc9375ccd65746a4abbfe70d5f375a132b7f9929)

## 0.1.1
2021-03-11

### Fixes

- ci/cd (9876982381e94045405714adfe4d3aab5332b3ad)

## 0.1.0
2021-03-11

### Features

- application base structure (2874338e65d22e20977246c0c509ba45b8f25a55)
- add initial db structure (64965e218de658e9d7ff9f55dc9bbb7ab9f8a8ef)
- add api for server (f061938e36db50dac4aabf4a39fde92b46d92f61)
- add api for application (b392ae843839d7c852aad5f2b3434742ddd3a786)
- add api for repository (e997908a06a58a5488c63ed5d56de704b84dd202)
- add swagger (cb0ae0b937ea35b2e57ec2e3413012b38c6d430f)


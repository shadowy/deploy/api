package model

import (
	"gorm.io/gorm"
	"strings"
)

type Environment struct {
	ID            string  `gorm:"primaryKey;size:36;<-:create" json:"id"`
	Name          string  `gorm:"size:300;index" json:"name"`
	Description   string  `gorm:"size:2000" json:"description"`
	Stack         string  `gorm:"size:300" json:"stack"`
	ServerID      string  `gorm:"size:36;index" json:"-"`
	ApplicationID string  `gorm:"size:36;index" json:"-"`
	Server        *Server `gorm:"foreignKey:id;references:server_id" json:"server"`
}

func (model *Environment) BeforeCreate(_ *gorm.DB) error {
	model.ID = strings.ToLower(model.ID)
	model.ApplicationID = strings.ToLower(model.ApplicationID)
	model.ServerID = strings.ToLower(model.Server.ID)
	return nil
}

func (model *Environment) BeforeUpdate(_ *gorm.DB) error {
	model.ID = strings.ToLower(model.ID)
	model.ApplicationID = strings.ToLower(model.ApplicationID)
	model.ServerID = strings.ToLower(model.Server.ID)
	return nil
}

package model

import (
	"gorm.io/gorm"
	"strings"
)

type Service struct {
	ID            string `gorm:"primaryKey;size:36;<-:create" json:"id"`
	Name          string `gorm:"size:300;index" json:"name"`
	Code          string `gorm:"size:100" json:"code"`
	Description   string `gorm:"size:2000" json:"description"`
	ApplicationID string `gorm:"index" json:"-"`
}

func (model *Service) BeforeCreate(_ *gorm.DB) error {
	model.ID = strings.ToLower(model.ID)
	model.ApplicationID = strings.ToLower(model.ApplicationID)
	return nil
}

func (model *Service) BeforeUpdate(_ *gorm.DB) error {
	model.ID = strings.ToLower(model.ID)
	model.ApplicationID = strings.ToLower(model.ApplicationID)
	return nil
}

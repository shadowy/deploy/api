package model

import (
	"gorm.io/gorm"
	"strings"
)

type Repository struct {
	ID          string  `gorm:"primaryKey;size:36;<-:create" json:"id"`
	Name        string  `gorm:"size:300;index" json:"name"`
	Description string  `gorm:"size:2000" json:"description"`
	URL         *string `gorm:"size:2000" json:"url"`
	Mask        *string `gorm:"size:2000" json:"mask"`
	User        *string `gorm:"size:1000" json:"user"`
	Password    *string `gorm:"size:1000" json:"password"`
}

func (model *Repository) BeforeCreate(_ *gorm.DB) error {
	model.ID = strings.ToLower(model.ID)
	return nil
}

func (model *Repository) BeforeUpdate(_ *gorm.DB) error {
	model.ID = strings.ToLower(model.ID)
	return nil
}

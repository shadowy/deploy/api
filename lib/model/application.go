package model

import (
	"gorm.io/gorm"
	"strings"
)

type Application struct {
	ID           string        `gorm:"primaryKey;size:36;<-:create" json:"id"`
	Name         string        `gorm:"size:300;index" json:"name"`
	Description  string        `gorm:"size:2000" json:"description"`
	Services     []Service     `gorm:"foreignKey:application_id;references:id;constraint:OnDelete:CASCADE;" json:"services"`
	Environments []Environment `gorm:"foreignKey:application_id;references:id;constraint:OnDelete:CASCADE" json:"environments"`
}

func (model *Application) BeforeCreate(_ *gorm.DB) error {
	model.ID = strings.ToLower(model.ID)
	return nil
}

func (model *Application) BeforeUpdate(_ *gorm.DB) error {
	model.ID = strings.ToLower(model.ID)
	return nil
}

func (model *Application) BeforeDelete(tx *gorm.DB) error {
	err := tx.Delete(Service{ApplicationID: model.ID}, "application_id = ?", model.ID).Error
	if err != nil {
		return err
	}
	return tx.Delete(Environment{ApplicationID: model.ID}, "application_id = ?", model.ID).Error
}

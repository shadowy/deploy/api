package model

import (
	"gorm.io/gorm"
	"strings"
)

type Server struct {
	ID          string  `gorm:"primaryKey;size:36;<-:create" json:"id"`
	Name        string  `gorm:"size:300;index" json:"name"`
	Description string  `gorm:"size:2000" json:"description"`
	URL         *string `gorm:"size:2000" json:"url"`
	Token       *string `gorm:"size:1000" json:"token"`
}

func (model *Server) BeforeCreate(_ *gorm.DB) error {
	model.ID = strings.ToLower(model.ID)
	return nil
}

func (model *Server) BeforeUpdate(_ *gorm.DB) error {
	model.ID = strings.ToLower(model.ID)
	return nil
}

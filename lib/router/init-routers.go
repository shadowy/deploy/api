package router

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
)

type initFunc func(*gin.RouterGroup) error

var initList = []initFunc{initApplicationController, initRepositoryController, initServerController}

func InitRouters(root *gin.RouterGroup) error {
	logrus.Debug("router.InitRouters")
	for i := range initList {
		init := initList[i]
		err := init(root)
		if err != nil {
			return err
		}
	}
	return nil
}

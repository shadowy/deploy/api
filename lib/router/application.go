package router

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"gitlab.com/shadowy/deploy/api/lib/data"
	"gitlab.com/shadowy/deploy/api/lib/model"
	"gitlab.com/shadowy/deploy/api/lib/router/response"
	"net/http"
)

type applicationController struct {
	repository *data.ApplicationRepository
}

func initApplicationController(root *gin.RouterGroup) error {
	logrus.Debug("router.initApplicationController")
	router := root.Group("application")
	ctrl := applicationController{repository: &data.ApplicationRepository{}}
	router.GET("", ctrl.find)
	router.GET("/:id", ctrl.get)
	router.POST("", ctrl.create)
	router.PUT("/:id", ctrl.update)
	router.DELETE("/:id", ctrl.delete)
	return nil
}

// @Description Get applications list
// @Tags Application
// @Accept  json
// @Produce  json
// @Param offset query int false "Offset"
// @Param limit query int false "Limit"
// @Param order query string false "order"
// @Param text query string false "Text for search"
// @Success 200 {object} []model.Application "Applications list"
// @Failure 406 {object} response.Error "Invalid parameters"
// @Failure 500 {object} response.Error "Internal error"
// @Router /application [get]
func (ctrl *applicationController) find(ctx *gin.Context) {
	offset, limit, order, text, err := parseGrid(ctx)
	if err != nil {
		ctx.JSON(http.StatusNotAcceptable, response.Error{Error: err.Error()})
		return
	}
	f := logrus.Fields{"offset": offset, "limit": limit, "text": text, "order": order}
	logrus.WithFields(f).Debug("router.serverController.get")
	res, err := ctrl.repository.Find(text, offset, limit, order)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, response.Error{Error: err.Error()})
		return
	}
	if res == nil {
		ctx.JSON(http.StatusNotFound, response.Error{Error: "Record not found"})
		return
	}
	ctx.JSON(http.StatusOK, res)
}

// @Description Get application
// @Tags Application
// @Accept  json
// @Produce  json
// @Param id path string true "Application ID"
// @Success 200 {object} model.Application	"Application"
// @Failure 404 {object} response.Error "Internal error"
// @Failure 500 {object} response.Error "Internal error"
// @Router /application/{id} [get]
func (ctrl *applicationController) get(ctx *gin.Context) {
	id := ctx.Param("id")
	f := logrus.Fields{"id": id}
	logrus.WithFields(f).Debug("router.serverController.get")
	res, err := ctrl.repository.Get(id)
	if err != nil {
		if err.Error() == "record not found" {
			ctx.JSON(http.StatusNotFound, response.Error{Error: err.Error()})
		} else {
			ctx.JSON(http.StatusInternalServerError, response.Error{Error: err.Error()})
		}
		return
	}
	ctx.JSON(http.StatusOK, res)
}

// @Description Create application
// @Tags Application
// @Accept  json
// @Produce  json
// @Param data body model.Application true "Application"
// @Success 204
// @Failure 404 {object} response.Error "Internal error"
// @Failure 500 {object} response.Error "Internal error"
// @Router /application [post]
func (ctrl *applicationController) create(ctx *gin.Context) {
	logrus.Debug("router.serverController.create")
	var m model.Application
	if err := ctx.BindJSON(&m); err != nil {
		ctx.JSON(http.StatusNotAcceptable, response.Error{Error: "Record not found"})
		return
	}
	err := ctrl.repository.Create(&m)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, response.Error{Error: err.Error()})
		return
	}
	ctx.Status(http.StatusNoContent)
}

// @Description Update application
// @Tags Application
// @Accept  json
// @Produce  json
// @Param id path string true "Application ID"
// @Param data body model.Application true "Application"
// @Success 204
// @Failure 404 {object} response.Error "Internal error"
// @Failure 500 {object} response.Error "Internal error"
// @Router /application/{id} [put]
func (ctrl *applicationController) update(ctx *gin.Context) {
	id := ctx.Param("id")
	f := logrus.Fields{"id": id}
	logrus.WithFields(f).Debug("router.serverController.update")
	var m model.Application
	if err := ctx.BindJSON(&m); err != nil {
		ctx.JSON(http.StatusNotAcceptable, response.Error{Error: "Record not found"})
		return
	}
	m.ID = id
	res, err := ctrl.repository.Update(&m)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, response.Error{Error: err.Error()})
		return
	}
	if !res {
		ctx.JSON(http.StatusNotFound, response.Error{Error: "Record not found"})
		return
	}
	ctx.Status(http.StatusNoContent)
}

// @Description Delete application
// @Tags Application
// @Accept  json
// @Produce  json
// @Param id path string true "Application ID"
// @Success 204
// @Failure 404 {object} response.Error "Internal error"
// @Failure 500 {object} response.Error "Internal error"
// @Router /application/{id} [delete]
func (ctrl *applicationController) delete(ctx *gin.Context) {
	id := ctx.Param("id")
	f := logrus.Fields{"id": id}
	logrus.WithFields(f).Debug("router.serverController.delete")
	res, err := ctrl.repository.Delete(id)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, response.Error{Error: err.Error()})
		return
	}
	if !res {
		ctx.JSON(http.StatusNotFound, response.Error{Error: "Record not found"})
		return
	}
	ctx.Status(http.StatusNoContent)
}

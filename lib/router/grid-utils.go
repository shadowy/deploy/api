package router

import (
	"github.com/gin-gonic/gin"
	"strconv"
)

func parseGrid(ctx *gin.Context) (offset, limit int, order, text string, err error) {
	strOffset := ctx.Query("offset")
	strLimit := ctx.Query("limit")
	order = ctx.Query("order")
	text = ctx.Query("text")
	if strOffset == "" {
		offset = 0
	} else {
		offset, err = strconv.Atoi(strOffset)
		if err != nil {
			return
		}
	}
	if strLimit == "" {
		limit = 30
	} else {
		limit, err = strconv.Atoi(strLimit)
		if err != nil {
			return
		}
	}
	return
}

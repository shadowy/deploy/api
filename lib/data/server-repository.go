package data

import (
	"github.com/sirupsen/logrus"
	"gitlab.com/shadowy/deploy/api/lib/model"
	"strings"
)

type ServerRepository struct{}

func (rep *ServerRepository) Get(id string) (res *model.Server, err error) {
	logrus.WithField("id", id).Debug("data.ServerRepository.Get")
	var r model.Server
	q := gormConnection.First(&r, "id = ?", strings.ToLower(id))
	if q.Error != nil {
		logrus.WithField("id", id).WithError(q.Error).Error("data.ServerRepository.Get")
		return nil, q.Error
	}
	return &r, nil
}

func (rep *ServerRepository) Find(text string, offset, limit int, order string) (res []*model.Server, err error) {
	f := logrus.Fields{"offset": offset, "limit": limit, "text": text, "order": order}
	logrus.WithFields(f).Debug("data.ServerRepository.Find")
	var r []*model.Server
	p := gormConnection.Limit(limit).Offset(offset)
	if order != "" {
		p = p.Order(order)
	}
	q := p.Find(&r, "lower(name) like ?", strings.ToLower(text)+"%")
	if q.Error != nil {
		logrus.WithFields(f).WithError(q.Error).Error("data.ServerRepository.Find")
		return nil, q.Error
	}
	return r, nil
}

func (rep *ServerRepository) Create(data *model.Server) error {
	logrus.Debug("data.ServerRepository.Create")
	data.ID = strings.ToLower(data.ID)
	q := gormConnection.Create(data)
	if q.Error != nil {
		logrus.WithField("id", data.ID).WithError(q.Error).Error("data.ServerRepository.Create")
		return q.Error
	}
	return nil
}

func (rep *ServerRepository) Update(data *model.Server) (re bool, err error) {
	logrus.Debug("data.ServerRepository.Update")
	data.ID = strings.ToLower(data.ID)
	q := gormConnection.Save(data)
	if q.Error != nil {
		logrus.WithField("id", data.ID).WithError(q.Error).Error("data.ServerRepository.Update")
		return false, q.Error
	}
	return q.RowsAffected != 0, nil
}

func (rep *ServerRepository) Delete(id string) (res bool, err error) {
	logrus.WithField("id", id).Debug("data.ServerRepository.Delete")
	id = strings.ToLower(id)
	q := gormConnection.Delete(&model.Server{ID: id}, "id = ?", id)
	if q.Error != nil {
		logrus.WithField("id", id).WithError(q.Error).Error("data.ServerRepository.Delete")
		return false, q.Error
	}
	return q.RowsAffected != 0, nil
}

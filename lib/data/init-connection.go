package data

import (
	gormLogrus "github.com/onrik/gorm-logrus"
	"github.com/sirupsen/logrus"
	"gitlab.com/shadowy/deploy/api/lib/model"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
	"reflect"
)

var gormConnection *gorm.DB
var models = []interface{}{model.Server{}, model.Repository{}, model.Service{}, model.Environment{}, model.Application{}}

func InitConnection(dbName string) error {
	logrus.WithField("dbName", dbName).Debug("data.initConnection")
	var err error
	gormConnection, err = gorm.Open(sqlite.Open(dbName), &gorm.Config{
		Logger: gormLogrus.New(),
	})
	if err != nil {
		logrus.WithField("dbName", dbName).WithError(err).Error("data.initConnection")
		return err
	}

	err = migrateDB()
	return err
}

func migrateDB() error {
	logrus.Debug("data.migrateDB")
	for i := range models {
		m := models[i]
		name := reflect.TypeOf(m).Name()
		logrus.WithField("name", name).Debug("data.migrateDB execute")
		err := gormConnection.AutoMigrate(m)
		if err != nil {
			logrus.WithField("name", name).WithError(err).Error("data.migrateDB execute")
		}
	}
	return nil
}

package data

import (
	"github.com/sirupsen/logrus"
	"gitlab.com/shadowy/deploy/api/lib/model"
	"strings"
)

type ApplicationRepository struct{}

func (rep *ApplicationRepository) Get(id string) (res *model.Application, err error) {
	logrus.WithField("id", id).Debug("data.ApplicationRepository.Get")
	var r model.Application
	q := gormConnection.Preload("Services").Preload("Environments").First(&r, "id = ?", strings.ToLower(id))
	if q.Error != nil {
		logrus.WithField("id", id).WithError(q.Error).Error("data.ApplicationRepository.Get")
		return nil, q.Error
	}
	return &r, nil
}

func (rep *ApplicationRepository) Find(text string, offset, limit int, order string) (res []*model.Application, err error) {
	f := logrus.Fields{"offset": offset, "limit": limit, "text": text, "order": order}
	logrus.WithFields(f).Debug("data.ApplicationRepository.Find")
	var r []*model.Application
	p := gormConnection.Preload("Services").Preload("Environments").Limit(limit).Offset(offset)
	if order != "" {
		p = p.Order(order)
	}
	q := p.Find(&r, "lower(name) like ?", strings.ToLower(text)+"%")
	if q.Error != nil {
		logrus.WithFields(f).WithError(q.Error).Error("data.ApplicationRepository.Find")
		return nil, q.Error
	}
	return r, nil
}

func (rep *ApplicationRepository) Create(data *model.Application) error {
	logrus.Debug("data.ApplicationRepository.Create")
	q := gormConnection.Create(data)
	if q.Error != nil {
		logrus.WithField("id", data.ID).WithError(q.Error).Error("data.ApplicationRepository.Create")
		return q.Error
	}
	return nil
}

func (rep *ApplicationRepository) Update(data *model.Application) (re bool, err error) {
	logrus.Debug("data.ApplicationRepository.Update")
	q := gormConnection.Save(data)
	if q.Error != nil {
		logrus.WithField("id", data.ID).WithError(q.Error).Error("data.ApplicationRepository.Update")
		return false, q.Error
	}
	return q.RowsAffected != 0, nil
}

func (rep *ApplicationRepository) Delete(id string) (res bool, err error) {
	logrus.WithField("id", id).Debug("data.ApplicationRepository.Delete")
	id = strings.ToLower(id)
	q := gormConnection.Delete(&model.Application{ID: id}, "id = ?", id)
	if q.Error != nil {
		logrus.WithField("id", id).WithError(q.Error).Error("data.ApplicationRepository.Delete")
		return false, q.Error
	}
	return q.RowsAffected != 0, nil
}

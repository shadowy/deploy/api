package data

import (
	"github.com/sirupsen/logrus"
	"gitlab.com/shadowy/deploy/api/lib/model"
	"strings"
)

type RepositoryRepository struct{}

func (rep *RepositoryRepository) Get(id string) (res *model.Repository, err error) {
	logrus.WithField("id", id).Debug("data.RepositoryRepository.Get")
	var r model.Repository
	q := gormConnection.First(&r, "id = ?", strings.ToLower(id))
	if q.Error != nil {
		logrus.WithField("id", id).WithError(q.Error).Error("data.ApplicationRepository.Get")
		return nil, q.Error
	}
	return &r, nil
}

func (rep *RepositoryRepository) Find(text string, offset, limit int, order string) (res []*model.Repository, err error) {
	f := logrus.Fields{"offset": offset, "limit": limit, "text": text, "order": order}
	logrus.WithFields(f).Debug("data.RepositoryRepository.Find")
	var r []*model.Repository
	p := gormConnection.Limit(limit).Offset(offset)
	if order != "" {
		p = p.Order(order)
	}
	q := p.Find(&r, "lower(name) like ?", strings.ToLower(text)+"%")
	if q.Error != nil {
		logrus.WithFields(f).WithError(q.Error).Error("data.RepositoryRepository.Find")
		return nil, q.Error
	}
	return r, nil
}

func (rep *RepositoryRepository) Create(data *model.Repository) error {
	logrus.Debug("data.RepositoryRepository.Create")
	q := gormConnection.Create(data)
	if q.Error != nil {
		logrus.WithField("id", data.ID).WithError(q.Error).Error("data.RepositoryRepository.Create")
		return q.Error
	}
	return nil
}

func (rep *RepositoryRepository) Update(data *model.Repository) (re bool, err error) {
	logrus.Debug("data.RepositoryRepository.Update")
	q := gormConnection.Save(data)
	if q.Error != nil {
		logrus.WithField("id", data.ID).WithError(q.Error).Error("data.RepositoryRepository.Update")
		return false, q.Error
	}
	return q.RowsAffected != 0, nil
}

func (rep *RepositoryRepository) Delete(id string) (res bool, err error) {
	logrus.WithField("id", id).Debug("data.RepositoryRepository.Delete")
	id = strings.ToLower(id)
	q := gormConnection.Delete(&model.Repository{ID: id}, "id = ?", id)
	if q.Error != nil {
		logrus.WithField("id", id).WithError(q.Error).Error("data.RepositoryRepository.Delete")
		return false, q.Error
	}
	return q.RowsAffected != 0, nil
}

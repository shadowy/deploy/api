module gitlab.com/shadowy/deploy/api

go 1.16

require (
	github.com/alecthomas/template v0.0.0-20190718012654-fb15b899a751 // indirect
	github.com/gin-gonic/gin v1.6.3 // indirect
	github.com/mattn/go-sqlite3 v1.14.6 // indirect
	github.com/onrik/gorm-logrus v0.3.0 // indirect
	github.com/sirupsen/logrus v1.8.1 // indirect
	github.com/swaggo/files v0.0.0-20190704085106-630677cd5c14 // indirect
	github.com/swaggo/gin-swagger v1.3.0 // indirect
	github.com/swaggo/swag v1.7.0 // indirect
	github.com/toorop/gin-logrus v0.0.0-20210225092905-2c785434f26f // indirect
	gorm.io/driver/sqlite v1.1.4 // indirect
	gorm.io/gorm v1.21.3 // indirect
)

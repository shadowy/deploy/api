package main

import (
	"flag"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
	ginLog "github.com/toorop/gin-logrus"
	"gitlab.com/shadowy/deploy/api/docs"
	"gitlab.com/shadowy/deploy/api/lib/data"
	"gitlab.com/shadowy/deploy/api/lib/router"
	"gitlab.com/shadowy/deploy/api/lib/settings/version"
)

var mode = flag.String("mode", "release", "GIN mode (release, debug, test)")
var bind = flag.String("bind", "0.0.0.0:8080", "Bind address and port")
var basePath = flag.String("base-path", "/api/", "base path (url)")
var db = flag.String("db", "deploy.db", "path and name of DB (sqlite3)")

// @title Swagger Deploy API
// @version 1.0
// @description This is a Deploy API Server
// @securityDefinitions.apikey ApiKeyAuth
// @in header
// @name Authorization
// @BasePath /
func main() {
	logrus.Info("deploy-api ver " + version.Version)
	initConfig()
	err := data.InitConnection(*db)
	if err != nil {
		return
	}
	initAPIServer()
}

func initConfig() {
	flag.Parse()
	logrus.WithFields(logrus.Fields{"mode": *mode, "bind": *bind, "base-path": *basePath, "db": *db}).Info("deploy-api settings")
	if *mode == "debug" || *mode == "test" {
		logrus.SetLevel(logrus.DebugLevel)
	}
}

func initAPIServer() {
	logrus.Debug("initAPIServer")
	gin.SetMode(*mode)
	server := gin.Default()
	server.Use(ginLog.Logger(logrus.New()), gin.Recovery())
	root := server.Group(*basePath)
	err := router.InitRouters(root)
	if err != nil {
		logrus.WithError(err).Error("deploy-api init routes")
		return
	}

	docs.SwaggerInfo.BasePath = *basePath
	docs.SwaggerInfo.Version = version.Version
	server.GET(*basePath+"swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
	err = server.Run(*bind)
	if err != nil {
		logrus.WithError(err).Error("initAPIServer")
	}
}

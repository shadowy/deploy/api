#!/bin/sh
MODE="${MODE:-"release"}"
BASE_PATH="${BASE_PATH:-"/api/"}"
echo "------------------------------------------------------"
echo "MODE=$MODE"
echo "BASE_PATH=$BASE_PATH"
echo "------------------------------------------------------"
/app/deploy-api --mode=$MODE --base-path=$BASE_PATH

binaries=deploy-api
module=deploy
version :=${VERSION:-"0.0.1"}

all: lint-full

test:
	@echo ">> test"

lint-full: lint card

card:
	@echo ">> card"
	@goreportcard-cli -v

lint:
	@echo ">> lint"
	@golangci-lint run

build: $(binaries)

$(binaries):
	@echo ">>build: $@ $(version)"
	@mkdir -p ./dist
	@env CGO_ENABLED=1 go build -ldflags="-X 'gitlab.com/shadowy/deploy/api/lib/settings/version.Version=$(version)'" -o ./dist/$@ ./cmd/$@/main.go
	@chmod 777 ./dist/$@


swagger:
	@echo ">>swagger"
	@swag init -g /cmd/deploy-api/main.go
